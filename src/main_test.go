package main

import (
	"os"
	"testing"
)

// package var used by benchmarks - prevents OS-level C compiler optimisations
var result string

func TestFormatTime(t *testing.T) {

	thisFunc := getFunction()

	// FORMAT: input seconds => expected string
	tables := []struct {
		inputSeconds int
		expected     string
	}{
		// from spec
		{1, "1 second"},
		{62, "1 minute and 2 seconds"},
		{3662, "1 hour, 1 minute and 2 seconds"},
		{94609440, "3 years and 24 minutes"},
		{273660, "3 days, 4 hours and 1 minute"},
		{0, "None"},

		// single returns
		{60, "1 minute"},
		{3600, "1 hour"},
		{86400, "1 day"},
		{31536000, "1 year"},
		{2, "2 seconds"},
		{120, "2 minutes"},
		{7200, "2 hours"},
		{172800, "2 days"},
		{63072000, "2 years"},

		// full range of formats
		{31626061, "1 year, 1 day, 1 hour, 1 minute and 1 second"},
		{63345906, "2 years, 3 days, 4 hours, 5 minutes and 6 seconds"},

		// large number (this is max int64 - anything larger will be caught by later test)
		{9223372036854775807, "292471208677 years, 195 days, 15 hours, 30 minutes and 7 seconds"},
	}

	for _, table := range tables {
		actual := FormatTime(table.inputSeconds)
		if actual != table.expected {
			t.Errorf("%s - Expected: %s | Actual: %s", thisFunc, table.expected, actual)
		}
	}

}

func TestFormatAndDisplayOutput(t *testing.T) {

	thisFunc := getFunction()

	// FORMAT: input years/days/hours/minutes/seconds => expected string
	tables := []struct {
		inputYears   int
		inputDays    int
		inputHours   int
		inputMinutes int
		inputSeconds int
		expected     string
	}{
		{0, 0, 0, 0, 1, "1 second"},
		{0, 0, 0, 0, 2, "2 seconds"},
		{0, 0, 0, 1, 0, "1 minute"},
		{0, 0, 0, 2, 0, "2 minutes"},
		{0, 0, 1, 0, 0, "1 hour"},
		{0, 0, 2, 0, 0, "2 hours"},
		{0, 1, 0, 0, 0, "1 day"},
		{0, 2, 0, 0, 0, "2 days"},
		{1, 0, 0, 0, 0, "1 year"},
		{2, 0, 0, 0, 0, "2 years"},
		{1, 1, 1, 1, 1, "1 year, 1 day, 1 hour, 1 minute and 1 second"},
		{2, 3, 4, 5, 6, "2 years, 3 days, 4 hours, 5 minutes and 6 seconds"},
	}

	for _, table := range tables {
		values := TimeValues{
			table.inputYears,
			table.inputDays,
			table.inputHours,
			table.inputMinutes,
			table.inputSeconds,
		}
		actual := formatAndDisplayOutput(values)
		if actual != table.expected {
			t.Errorf("%s - Expected: %s | Actual: %s", thisFunc, table.expected, actual)
		}
	}

}

func TestConvertSecondsToForecast(t *testing.T) {

	thisFunc := getFunction()

	// FORMAT: input seconds => calculated TimeValues
	tables := []struct {
		inputSeconds    int
		expectedYears   int
		expectedDays    int
		expectedHours   int
		expectedMinutes int
		expectedSeconds int
	}{
		// 1 or 2 Y/D/H/M/S
		{1, 0, 0, 0, 0, 1},
		{2, 0, 0, 0, 0, 2},
		{60, 0, 0, 0, 1, 0},
		{120, 0, 0, 0, 2, 0},
		{3600, 0, 0, 1, 0, 0},
		{7200, 0, 0, 2, 0, 0},
		{86400, 0, 1, 0, 0, 0},
		{172800, 0, 2, 0, 0, 0},
		{31536000, 1, 0, 0, 0, 0},
		{63072000, 2, 0, 0, 0, 0},

		// 2 of Y/D/H/M/S
		{61, 0, 0, 0, 1, 1},
		{3601, 0, 0, 1, 0, 1},
		{86401, 0, 1, 0, 0, 1},
		{31536001, 1, 0, 0, 0, 1},
		{3660, 0, 0, 1, 1, 0},
		{86460, 0, 1, 0, 1, 0},
		{31536060, 1, 0, 0, 1, 0},
		{90000, 0, 1, 1, 0, 0},
		{31539600, 1, 0, 1, 0, 0},
		{31622400, 1, 1, 0, 0, 0},

		// 3 of Y/D/H/M/S
		{3661, 0, 0, 1, 1, 1},
		{86461, 0, 1, 0, 1, 1},
		{31536061, 1, 0, 0, 1, 1},
		{90060, 0, 1, 1, 1, 0},
		{31539660, 1, 0, 1, 1, 0},
		{31626000, 1, 1, 1, 0, 0},

		// 4 of Y/D/H/M/S
		{90061, 0, 1, 1, 1, 1},
		{31539661, 1, 0, 1, 1, 1},
		{31626060, 1, 1, 1, 1, 0},

		// all 5
		{31626061, 1, 1, 1, 1, 1},
	}

	for _, table := range tables {
		actual := convertSecondsToForecast(table.inputSeconds)
		if actual.Years != table.expectedYears {
			t.Errorf("%s(%d): Years   - Expected %d, Actual %d", thisFunc, table.inputSeconds, table.expectedYears, actual.Years)
		}
		if actual.Days != table.expectedDays {
			t.Errorf("%s(%d): Days    - Expected %d, Actual %d", thisFunc, table.inputSeconds, table.expectedDays, actual.Days)
		}
		if actual.Hours != table.expectedHours {
			t.Errorf("%s(%d): Hours   - Expected %d, Actual %d", thisFunc, table.inputSeconds, table.expectedHours, actual.Hours)
		}
		if actual.Minutes != table.expectedMinutes {
			t.Errorf("%s(%d): Minutes - Expected %d, Actual %d", thisFunc, table.inputSeconds, table.expectedMinutes, actual.Minutes)
		}
		if actual.Seconds != table.expectedSeconds {
			t.Errorf("%s(%d): Seconds - Expected %d, Actual %d", thisFunc, table.inputSeconds, table.expectedSeconds, actual.Seconds)
		}
	}

}

// This test deliberately generates advisory output messages to screen
func TestHandleArguments(t *testing.T) {

	thisFunc := getFunction()

	// FORMAT: os.Args[] => expected return code
	tables := []struct {
		arg0     string
		arg1     string
		expected int
	}{
		{"valid-int", "1", 1},
		{"max-int", "9223372036854775807", 9223372036854775807},
		{"help", "-h", -1},
		{"missing-input", "", -2},
		{"negative-int", "-1", -3},
		{"string", "test", -2},
		{"over-max-int", "9223372036854775808", -2},
	}

	for _, table := range tables {
		os.Args = []string{table.arg0, table.arg1}
		actual := handleArguments()
		if actual != table.expected {
			t.Errorf("%s(%s) - Expected: %d | Actual: %d", thisFunc, table.arg0, table.expected, actual)
		}
	}

}

func BenchmarkFormatTimeZero(b *testing.B) {
	benchmarkFormatTime(0, b)
}
func BenchmarkFormatTimeSmall(b *testing.B) {
	benchmarkFormatTime(1, b)
}

func BenchmarkFormatTimeMedium(b *testing.B) {
	benchmarkFormatTime(31536000, b)
}

func BenchmarkFormatTimeLarge(b *testing.B) {
	benchmarkFormatTime(1234567890123, b)
}

func BenchmarkFormatTimeMega(b *testing.B) {
	benchmarkFormatTime(9223372036854775807, b)
}

func benchmarkFormatTime(i int, b *testing.B) {

	var r string

	// NB: benchmark must generate a consistent time to avoid looping!
	for n := 0; n < b.N; n++ {
		r = FormatTime(i)
	}

	result = r

}
