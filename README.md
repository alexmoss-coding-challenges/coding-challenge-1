# coding-challenge-1

Function that converts seconds into a time in a human-readable format. For more detail see [SPEC.md].

Opted to write it in Go because:

1. I have just started learning it
2. I like that it neatly packages up into a single executable
3. It's different!
4. It's fun!

---

## Usage 

You need either `docker` or `go` on your local machine - both are easy to install on most common OSes.

### Running with Docker 

You can either:

1. Pull the image from docker hub and run it: `docker run mosstech/format-time [seconds]`.

Or:

2. You can run `./go build-docker` to build the docker image locally and then run it with `docker run format-time <time-in-seconds>"`.

### Running with Go

There is a wrapper script (`./go` in `bash`) to make this easier:
- `./go run`- run go locally without building
- `./go test` - run unit tests and benchmarks
- `./go build` - build binary for mac/linux/win (amd64)
- `./go build-docker` - build the docker image locally and run smoke tests

With a built go binary, you can invoke it with `cd bin/ && ./format-time [int-seconds]`.

---

## Improvements

- [ ] Tests are a bit spread out - collapse together into package vars?
- [ ] *This needs GOPATH fiddling to be portable* Quite verbose now - restructure into packages?
- [ ] Hide log output from handleArguments tests / figure out how to do negative testing
- [ ] Use a builder image for cleaner CI

- [ ] Enhancement: months? Could be satisfied by time difference using Time functions
- [ ] Enhancement: leap years? Could be satisfied by time difference using Time functions
