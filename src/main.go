package main

import (
	"fmt"
	"log"
	"os"
	"runtime"
	"strconv"
	"strings"
)

// TimeValues is used to pass around the calculated years/days/hours/minutes/seconds
type TimeValues struct {
	Years   int
	Days    int
	Hours   int
	Minutes int
	Seconds int
}

func main() {

	var output string

	totalSeconds := handleArguments()

	output = FormatTime(totalSeconds)

	// print to screen, but could be wrapped in alternatives
	fmt.Println(output)

}

// FormatTime receives an integer number of seconds and returns a string in human-readable format
func FormatTime(totalSeconds int) string {

	var output string
	var values TimeValues

	if totalSeconds == 0 {
		output = "None"
	} else {
		values = convertSecondsToForecast(totalSeconds)
		output = formatAndDisplayOutput(values)
	}

	return output

}

func formatAndDisplayOutput(calcValues TimeValues) string {

	var output []string

	if calcValues.Years == 1 {
		output = append(output, fmt.Sprintf("%d year", calcValues.Years))
	} else if calcValues.Years > 0 {
		output = append(output, fmt.Sprintf("%d years", calcValues.Years))
	}

	if calcValues.Days == 1 {
		output = append(output, fmt.Sprintf("%d day", calcValues.Days))
	} else if calcValues.Days > 0 {
		output = append(output, fmt.Sprintf("%d days", calcValues.Days))
	}

	if calcValues.Hours == 1 {
		output = append(output, fmt.Sprintf("%d hour", calcValues.Hours))
	} else if calcValues.Hours > 0 {
		output = append(output, fmt.Sprintf("%d hours", calcValues.Hours))
	}

	if calcValues.Minutes == 1 {
		output = append(output, fmt.Sprintf("%d minute", calcValues.Minutes))
	} else if calcValues.Minutes > 0 {
		output = append(output, fmt.Sprintf("%d minutes", calcValues.Minutes))
	}

	if calcValues.Seconds == 1 {
		output = append(output, fmt.Sprintf("%d second", calcValues.Seconds))
	} else if calcValues.Seconds > 0 {
		output = append(output, fmt.Sprintf("%d seconds", calcValues.Seconds))
	}

	// join with commas then replace last comma with 'and'
	out := fmt.Sprint(strings.Join(output, ", "))
	i := strings.LastIndex(out, ",")
	if i > 0 {
		out = out[:i] + strings.Replace(out[i:], ",", " and", 1)
	}

	return out

}

func convertSecondsToForecast(inputSeconds int) TimeValues {

	minutes, seconds := calcTimeValue(60, inputSeconds)
	hours, minutes := calcTimeValue(60, minutes)
	days, hours := calcTimeValue(24, hours)
	years, days := calcTimeValue(365, days)

	calcValues := TimeValues{years, days, hours, minutes, seconds}

	return calcValues

}

func calcTimeValue(unit int, inSeconds int) (int, int) {
	value := inSeconds / unit
	remainder := inSeconds % unit
	return value, remainder
}

func handleArguments() int {

	var inputSeconds int

	// first arg is the program's path itself
	if len(os.Args) == 1 || os.Args[1] == "-h" || os.Args[1] == "-help" {

		displayHelp()
		inputSeconds = -1

	} else {

		userInput, err := strconv.Atoi(os.Args[1])
		if err != nil {
			// exceeding max int64 (9223372036854775807) will also fall through to here
			log.Println("You must specify an integer number of seconds")
			inputSeconds = -2
		} else {
			if userInput < 0 {
				log.Println("You must specify a positive integer")
				inputSeconds = -3
			} else {
				inputSeconds = userInput
			}
		}

	}

	return inputSeconds

}

func displayHelp() {
	fmt.Printf("\n USAGE: %s [seconds (int)]\n\n", os.Args[0])
	fmt.Println("        Will display a human-readable time for the number of seconds specified")
}

func getFunction() string {
	pc := make([]uintptr, 15)
	n := runtime.Callers(2, pc)
	frames := runtime.CallersFrames(pc[:n])
	frame, _ := frames.Next()
	functionParts := strings.Split(frame.Function, ".")
	function := functionParts[len(functionParts)-1]
	return function
}
